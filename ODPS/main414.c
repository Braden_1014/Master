#include <stdio.h>

int mix(char *A, char *B, char *out);

int main(){

	char A[120], B[120], out[240];

	printf("Enter string A: ");
	fgets(A, 120, stdin);
	printf("Enter string B(must be same length as string A): ");
	fgets(B, 120, stdin);
//	strcpy(A,"ABC");
//	strcpy(B,"123");
	mix(A, B, out);
	printf("Output = <%s>\n", out);
	return 0;
}
