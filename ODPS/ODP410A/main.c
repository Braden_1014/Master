#include <stdio.h>

int inc(int);

int main()
{
	int y;

	y=inc(10);
	printf("inc(10) returned %d\n", y);
	
	y=inc(-255);
	printf("inc(-255) returned %d\n", y);
}

