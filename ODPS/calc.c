#include <stdio.h>
#include <string.h>

int calc(char *in){

	int num1 = 0, num2 = 0, result, answer;
	char op;

result = sscanf(in,"%d%c%d",&num1,&op,&num2);
	
	if(result == 3)
	{
		if(op == '+')
		{
			answer = num1 + num2;
			return answer;
		}
		else if(op == '-')
		{
			answer = num1 - num2;
			return answer;
		}
		else if(op == '*')
		{
			answer = num1 * num2;
			return answer;
		}
		else if(op == '/')
		{
			answer = num1 / num2;
			return answer;
		}
		else
		{
			return -2;
		}
	}
	else
	{
		return -1;
	}


}
