#include <stdio.h>
#include <math.h>

int main(int argc, char **argv) 
{
	double n, result, valid;
	char temp[100];

	if (argc == 2)
	{
		valid = sscanf(argv[1], "%lf%s", &n, temp);

		if(valid == 1)
		{
			if(n >= 0)
			{
				result = sqrt(n);
				printf("%lf\n",result);
			}
			else if(n < 0)
			{
				printf("NEG\n");
				return 0;
			}
		}
		else
		{
			printf("INV\n");
			return 0;
		}
	}
	else
	{
		printf("ERR\n");
		return 0;
	}

return 0;
}
