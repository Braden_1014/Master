#include<stdio.h>

struct node {
	int data;
	int next;
	int valid;
};

int get_node(struct node *);

void main(){

	struct node stuff[100];
	get_node(stuff);

	for(int i=1;i<=99;i++){

		int retval=get_node(stuff);
		printf("%d\n", retval);
	}
return;
}
