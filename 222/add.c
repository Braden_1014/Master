#include<stdio.h>
#include<stdlib.h>

struct tnode{
	int data;
	struct tnode *left;
	struct tnode *right;
};

struct tnode* add(struct tnode *root, int n)
{
	struct tnode *new;
	new=malloc(sizeof(struct tnode));
	new->data=n;
	new->left=NULL;
	new->right=NULL;
	
	if(n < root->data)
	{
		root->left=new;
	}
	else if(n > root->data)
	{
		root->right=new;
	}
return root;
}
