#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
	struct node *next;
};

struct node* build(int x, int y, int z)
{
	struct node *sentinel, *nx, *ny, *nz;
	sentinel=malloc(sizeof(struct node));
	nx=malloc(sizeof(struct node));
	ny=malloc(sizeof(struct node));
	nz=malloc(sizeof(struct node));

	nx->data=x;
	ny->data=y;
	nz->data=z;

	sentinel->next=nx;
	nx->next=ny;
	ny->next=nz;

	return sentinel;
}
