#include<stdio.h>
#include<stdlib.h>


struct node{
	int data;
	struct node *left;
	struct node *right;
};

int largest(struct node *root)
{
	struct node *start;
	start=root->left;
	while(start->right != NULL)
	{	
		start=start->right;
	}
	if(start->right == NULL)
	{
		return start->data;
	}
}
  
