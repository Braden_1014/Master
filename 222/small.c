#include<stdio.h>
#include<stdlib.h>

struct tnode{
	int data;
	struct tnode *left;
	struct tnode *right;
};

struct tnode* small(int root,int left,int right)
{
	struct tnode *rootnode,*leftchild,*rightchild;
	rootnode=malloc(sizeof(struct tnode));
	leftchild=malloc(sizeof(struct tnode));
	rightchild=malloc(sizeof(struct tnode));
	rootnode->data=root;
	leftchild->data=left;
	rightchild->data=right;
	rootnode->left=leftchild;
	rootnode->right=rightchild;
	leftchild->left=NULL;
	leftchild->right=NULL;
	rightchild->left=NULL;
	rightchild->right=NULL;
	return rootnode;
}
