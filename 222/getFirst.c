#include<stdio.h>

#define MYNULL 0

struct node{
	int data;
	int next;
	int valid;
};

int getFirst(struct node *LL, int *error)
{
	int i = LL[0].next;

	if(i == MYNULL)
	{
		*error=1;
		return(0);
	}
	else if(i != MYNULL)
	{
		struct node first=LL[i];
		*error=0;
		return(first.data);
	}
}
